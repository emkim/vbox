@echo off

SET nat=0
SET bridge=0
SET user=idu
SET debian_iso="T:\debian-12.9.0-amd64-netinst.iso"

SET vmname="Debian_%user%_0"

mkdir D:\poubelle\idu

REM Create VirtualBox VM :
"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" createvm --name %vmname% --ostype Debian_64 --basefolder D:\poubelle\idu --register
"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" modifyvm %vmname% --cpus 1 --memory 2048 --vram 16

"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" modifyvm %vmname% --nic1 nat
"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" modifyvm %vmname% --natpf1 "ssh,tcp,,22,,22"


"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" modifyvm %vmname% --audio-enabled off --audio-driver none
"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" createhd --filename ./%vmname%.vdi --size 20480 --variant Standard
"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" storagectl %vmname% --name "SATA Controller" --add sata --bootable on
"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" storageattach %vmname% --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium ./%vmname%.vdi
"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" storagectl %vmname% --name "IDE Controller" --add ide
"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" storageattach %vmname% --storagectl "IDE Controller" --port 0  --device 0 --type dvddrive --medium "%debian_iso%"
"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" unattended install %vmname% --iso "%debian_iso%" --script-template ./preseed.cfg   --locale=en_US --country=FR --user=%user% --password=Pwd%user%12 --time-zone "Europe/Paris" --extra-install-kernel-parameters "auto=true preseed/file=/cdrom/preseed.cfg priority=critical quiet splash noprompt noshell automatic-ubiquity debian-installer/locale=en_US keyboard-configuration/layoutcode=fr languagechooser/language-name=English localechooser/supported-locales=en_US.UTF-8 countrychooser/shortlist=US ipv6.disable=1  --"

"c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" startvm %vmname%