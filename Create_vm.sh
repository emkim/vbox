#!/bin/bash
# VBoxManage="/mnt/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe"
# https://gemmei.ftp.acc.umu.se/debian-cd/current/amd64/iso-cd/debian-12.5.0-amd64-netinst.iso
# https://gitlab.com/emkim/Vbox/-/raw/main/preseed.cfg
# /mnt/c/Windows/System32/WindowsPowerShell/v1.0/powershell.exe 'Get-NetAdapter -Name * -Physical | Format-List -Property "InterfaceDescription"' | grep "InterfaceDescription" | cut -d " " -f 3-

nat=0
bridge=0

VBoxManage() {
    /mnt/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe "$@"
}
export -f VBoxManage

# Check parameters
while [ -n "$1" ] ; do
	case "$1" in
		-b|--bridge)
			bridge=1
			adpater="$2"
			shift
			;;
		-n|--nat)
			nat=1
			;;
		-u|--user)
			user="$2"
			shift
			;;
		-*) echo "Bad option $1"
			exit 1
			;;
	esac
	shift
done

if [ -z ${user} ]; then
	echo "User must be set with -u or --user"
	exit 1
fi

vmname="Debian_${user^^}_0"



# Dowload preseed.cfg
[[ ! -f ./preseed.cfg ]] && wget https://gitlab.com/emkim/VBox/-/raw/main/preseed.cfg


# Create VirtualBox VM :
VBoxManage createvm --name ${vmname} --ostype Debian_64 --register
VBoxManage modifyvm ${vmname} --cpus 1 --memory 2048 --vram 16

# Set network if needed :
if [ ${nat} -eq 1 ]; then
	VBoxManage modifyvm ${vmname} --nic1 nat
elif [ ${bridge} -eq 1 ]; then
	VBoxManage modifyvm ${vmname} --nic1 bridged --bridgeadapter1 "$(echo "${adpater}")"
else
	echo ""
	echo "===================================="
	echo "Use nat or bridge network : "
	echo "1) nat"
	echo "2) bridge"
	while true; do
		read -p "nat or bridge ? " network_type
		case ${network_type} in
			[1])
				VBoxManage modifyvm ${vmname} --nic1 nat
				VBoxManage modifyvm ${vmname} --natpf1 "ssh,,22,,22"
				break
				;;
			[2])
				# List physical adapters :
				echo ""
				readarray -t adapters < <(/mnt/c/Windows/System32/WindowsPowerShell/v1.0/powershell.exe 'Get-NetAdapter -Name * -Physical | Format-List -Property "InterfaceDescription"' | grep "InterfaceDescription" | cut -d " " -f 3-)
				echo "Please select an adapter :"
				select choice in "${adapters[@]}"; do
					[[ -n $choice ]] || { echo "Invalid choice. Please try again." >&2; continue; }
					break # valid choice was made; exit prompt.
				done
				VBoxManage modifyvm ${vmname} --nic1 bridged --bridgeadapter1 "$(echo "${choice}" | dos2unix | cat)"
				break
				;;
			* ) echo "Please answer 1 or 2.";;
		esac
	done
fi

VBoxManage modifyvm ${vmname} --audio-enabled off --audio-driver none
VBoxManage createhd --filename ./${vmname}.vdi --size 20480 --variant Standard
#VBoxManage createhd --filename /mnt/c/Users/$USER/VirtualBox\ VMs/${vmname}/${vmname}.vdi --size 5120
VBoxManage storagectl ${vmname} --name "SATA Controller" --add sata --bootable on
VBoxManage storageattach ${vmname} --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium ./${vmname}.vdi
#VBoxManage storageattach ${vmname} --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium /mnt/c/Users/$USER/VirtualBox\ VMs/${vmname}/${vmname}.vdi
VBoxManage storagectl ${vmname} --name "IDE Controller" --add ide
VBoxManage storageattach ${vmname} --storagectl "IDE Controller" --port 0  --device 0 --type dvddrive --medium "c:\Users\\$USER\Downloads\debian-12.5.0-amd64-netinst.iso"
# VBoxManage storageattach ${vmname} --storagectl "IDE Controller" --port 0  --device 0 --type dvddrive --medium /mnt/c/Users/$USER/Downloads/debian-12.5.0-amd64-netinst.iso


VBoxManage unattended install ${vmname} --iso "c:\Users\\$USER\Downloads\debian-12.5.0-amd64-netinst.iso" --script-template ./preseed.cfg   --locale=en_US --country=FR --user=${user} --password=Pwd${user}12 --time-zone "Europe/Paris" --extra-install-kernel-parameters "auto=true preseed/file=/cdrom/preseed.cfg priority=critical quiet splash noprompt noshell automatic-ubiquity debian-installer/locale=en_US keyboard-configuration/layoutcode=fr languagechooser/language-name=English localechooser/supported-locales=en_US.UTF-8 countrychooser/shortlist=US ipv6.disable=1  --"

VBoxManage startvm ${vmname}
